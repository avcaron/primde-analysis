from setuptools import setup

setup(
    name='primde-analysis',
    version='v1.0',
    packages=['lib'],
    url='',
    license='',
    author='Alex Valcourt Caron',
    author_email='alex.valcourt.caron@usherbrooke.ca',
    description='Analysis code for Magic-Monkey run on Prime-DE'
)
