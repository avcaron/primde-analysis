import os

from .variation_coeff import extract_subset_cvs


def get_sessions_data(subject, data_json):
    subject_tissue_segmentations = []
    subject_wm_segmentations = []
    subject_data_paths = []
    subject_data_tags = []

    subject_meta = data_json["metadata"]["subjects"][subject]

    for session in subject_meta["sessions"]:
        session_data = data_json["datapoints"][session]
        tag = "{}_{}".format(
            subject_meta["subject"], session_data["session"]
        )

        subject_tissue_segmentations.append(
            os.path.join(
                session_data["data"],
                "segmentation",
                "{}_segmentation.nii.gz".format(tag)
            )
        )
        subject_wm_segmentations.append(
            os.path.join(
                session_data["data"],
                "segmentation",
                "{}_wm_segmentation_atlas.nii.gz".format(tag)
            )
        )

        subject_data_paths.append(session_data["data"])
        subject_data_tags.append(tag)

    return subject_tissue_segmentations, \
        subject_wm_segmentations, \
        subject_data_paths, \
        subject_data_tags


def compute_subjects_cv_multi(
    output_tag, subjects, data_json, wm_labels, model_list
):
    subjects_tissue_segmentation = []
    subjects_wm_segmentation = []
    subjects_data_paths = []
    subjects_data_tags = []

    for subject in subjects:
        sts, swm, sdp, sdt = get_sessions_data(subject, data_json)
        subjects_tissue_segmentation.append(sts)
        subjects_wm_segmentation.append(swm)
        subjects_data_paths.append(sdp)
        subjects_data_tags.append(sdt)

    return output_tag, extract_subset_cvs(
        subjects_data_paths, subjects_data_tags,
        subjects_tissue_segmentation, subjects_wm_segmentation,
        wm_labels, model_list
    )


def compute_subject_cv_multi(
    project, subject, data_json, wm_labels, model_list
):
    (
        subject_tissue_segmentations, subject_wm_segmentations,
        subject_data_paths, subject_data_tags
    ) = get_sessions_data(subject, data_json)

    subject_meta = data_json["metadata"]["subjects"][subject]

    return "{}:{}".format(project, subject_meta["subject"]), \
        extract_subset_cvs(
            [subject_data_paths], [subject_data_tags],
            [subject_tissue_segmentations], [subject_wm_segmentations],
            wm_labels, model_list, False
        )
