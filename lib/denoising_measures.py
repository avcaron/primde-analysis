from itertools import combinations
from scipy.ndimage import (binary_closing,
                           binary_dilation,
                           generate_binary_structure)

import numpy as np
import pandas as pd


def estimate_background_noise_mask(data):
    hist, bins = np.histogram(data, 1000)
    max_val_idx = np.argmax(hist)
    hist, bins = hist[max_val_idx:], bins[max_val_idx:]
    first_minima_intensity = bins[0]
    for i, (h, b) in enumerate(zip(hist[1:], bins[1:])):
        if h < hist[i]:
            first_minima_intensity = b
        else:
            break

    noise_mask = np.ones_like(data)
    noise_mask[data > first_minima_intensity] = 0
    noise_mask = noise_mask.astype(bool)

    structure = np.zeros((5, 5, 5))
    structure[2, 2, 2] = 1
    tmp_struct = generate_binary_structure(3, 1)
    structure = binary_dilation(structure, structure=tmp_struct)
    structure = binary_dilation(structure, structure=tmp_struct)

    return binary_closing(noise_mask, structure.astype(bool), origin=(2, 2, 2))


def estimate_noise_from_multi_b0(b0s):
    noise_maps = np.zeros(
        b0s.shape[:-1] + (int(b0s.shape[-1] * (b0s.shape[-1] - 1) / 2),)
    )
    for i, pair in enumerate(combinations(range(0, b0s.shape[-1]), 2)):
        noise_maps[..., i] = b0s[..., pair[0]] - b0s[..., pair[1]]

    return np.mean(noise_maps, axis=-1)


def get_noise_measures(clean_dwi, clean_bval_mask, noisy_dwi, noisy_bval_mask):
    clean_noise_map, noisy_noise_map = None, None
    clean_background_std, noisy_background_std = None, None

    n_bval = min(np.count_nonzero(clean_bval_mask), np.count_nonzero(noisy_bval_mask))

    if n_bval > 1:
        clean_noise_map = estimate_noise_from_multi_b0(
            clean_dwi[..., clean_bval_mask]
        )
        noisy_noise_map = estimate_noise_from_multi_b0(
            noisy_dwi[..., noisy_bval_mask]
        )
    else:
        clean_background_noise_mask = estimate_background_noise_mask(
            clean_dwi[..., clean_bval_mask].squeeze()
        )
        clean_background_std = np.std(
            clean_dwi[clean_background_noise_mask][..., clean_bval_mask]
        )
        noisy_background_std = np.std(
            noisy_dwi[clean_background_noise_mask][..., noisy_bval_mask]
        )

    return clean_noise_map, noisy_noise_map, \
        clean_background_std, noisy_background_std


def get_snr_table(
    clean_dwi, clean_bval_mask, noisy_dwi, noisy_bval_mask, segmentation, labels
):
    unique_tissues = labels.keys()
    n_b0 = min(np.count_nonzero(clean_bval_mask), np.count_nonzero(noisy_bval_mask))

    clean_noise_map, noisy_noise_map, clean_std, noisy_std = \
        get_noise_measures(clean_dwi, clean_bval_mask, noisy_dwi, noisy_bval_mask)

    snr_array = np.zeros((2, len(unique_tissues)))
    for i, tissue in enumerate(unique_tissues):
        tissue_mask = segmentation == tissue
        snr_array[0, i] = np.mean(clean_dwi[tissue_mask][..., clean_bval_mask])
        snr_array[1, i] = np.mean(noisy_dwi[tissue_mask][..., noisy_bval_mask])

        if n_b0 > 1:
            snr_array[0, i] /= np.std(clean_noise_map[tissue_mask])
            snr_array[1, i] /= np.std(noisy_noise_map[tissue_mask])
        else:
            snr_array[0, i] /= clean_std
            snr_array[1, i] /= noisy_std

    snr_array *= np.sqrt(1. - np.pi / 4.)

    return pd.DataFrame(snr_array, ["clean", "noisy"], labels.values())


def _weighted_std(values, weights):
    avg = np.average(values, weights=weights)
    return np.sqrt(np.average((values - avg) ** 2., weights=weights))


def get_cnr_table(
    clean_dwi, clean_bval_mask, noisy_dwi, noisy_bval_mask, wm_segmentation,
    labels, gm_mask, gm_weights, wm_weights
):
    unique_tissues = labels.keys()
    n_b0 = min(np.count_nonzero(clean_bval_mask), np.count_nonzero(noisy_bval_mask))

    clean_noise_map, noisy_noise_map, clean_std, noisy_std = \
        get_noise_measures(clean_dwi, clean_bval_mask, noisy_dwi, noisy_bval_mask)

    mean_clean_gm_signal = np.average(
        np.mean(clean_dwi[gm_mask][..., clean_bval_mask], axis=-1),
        weights=gm_weights[gm_mask]
    )
    mean_noisy_gm_signal = np.average(
        np.mean(noisy_dwi[gm_mask][..., noisy_bval_mask], axis=-1),
        weights=gm_weights[gm_mask]
    )

    clean_gm_noise_std_sq, noisy_gm_noise_std_sq = None, None
    if n_b0 > 1:
        clean_gm_noise_std_sq = _weighted_std(
            clean_noise_map[gm_mask],
            gm_weights[gm_mask]
        ) ** 2.
        noisy_gm_noise_std_sq = _weighted_std(
            noisy_noise_map[gm_mask],
            gm_weights[gm_mask]
        ) ** 2.

    cnr_array = np.zeros((2, len(unique_tissues)))
    for i, tissue in enumerate(unique_tissues):
        tissue_mask = wm_segmentation == tissue
        tissue_weights = wm_weights[tissue_mask]

        if len(tissue_weights) == 0:
            cnr_array[:, i] = np.nan
            continue

        cnr_array[0, i] = np.abs(
            np.average(
                np.mean(clean_dwi[tissue_mask][..., clean_bval_mask], axis=-1),
                weights=tissue_weights
            ) - mean_clean_gm_signal
        )
        cnr_array[1, i] = np.abs(
            np.average(
                np.mean(noisy_dwi[tissue_mask][..., noisy_bval_mask], axis=-1),
                weights=tissue_weights
            ) - mean_noisy_gm_signal
        )

        if n_b0 > 1:
            cnr_array[0, i] /= np.sqrt(
                clean_gm_noise_std_sq + _weighted_std(
                    clean_noise_map[tissue_mask], tissue_weights
                ) ** 2.
            )
            cnr_array[1, i] /= np.sqrt(
                noisy_gm_noise_std_sq + _weighted_std(
                    noisy_noise_map[tissue_mask], tissue_weights
                ) ** 2.
            )
        else:
            cnr_array[0, i] /= clean_std
            cnr_array[1, i] /= noisy_std

    return pd.DataFrame(cnr_array, ["clean", "noisy"], labels.values())
