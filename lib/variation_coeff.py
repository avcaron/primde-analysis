import os

import nibabel as nib
import numpy as np
import pandas as pd
import seaborn as sns


def _average_repetitions(data_list):
    ref = nib.load(data_list[0])
    data = ref.get_fdata()
    if len(data_list) == 1:
        return data

    data = data[..., None]
    for dataset in data_list[1:]:
        data = np.concatenate(
            (data, nib.load(dataset).get_fdata()[..., None]), axis=-1
        )

    data = np.mean(data, axis=-1)

    if np.issubdtype(ref.get_data_dtype(), np.integer):
        data = np.round(data)

    return data.astype(ref.get_data_dtype())


def _compute_cv_on(data, segmentations, labels, avg_rep=True):
    if avg_rep:
        datapoints = [_average_repetitions(dt) for dt in data]
        segmentations = [_average_repetitions(seg) for seg in segmentations]
    else:
        datapoints = [nib.load(dt).get_fdata()
                      for dt_list in data for dt in dt_list]
        segmentations = [nib.load(seg).get_fdata()
                         for seg_list in segmentations for seg in seg_list]

    cvs = np.zeros((1, len(labels.keys())))
    for i, label in enumerate(labels.keys()):
        data_list = []
        for data, seg in zip(datapoints, segmentations):
            label_data = data[seg == label]
            if len(label_data) > 0:
                data_list.append(np.mean(label_data))

        if len(data_list) > 0:
            cvs[0, i] = np.std(data_list) / np.mean(data_list)

    return pd.DataFrame(cvs, columns=labels.values())


def extract_subset_cvs(
    data_paths, data_tags, tissue_segmentation,
    wm_segmentation, wm_labels, model_list, avg_rep=True
):
    subset_dict = {m: {} for m in model_list.keys()}
    for model, metrics_list in model_list.items():
        for metric_info in metrics_list:
            for metric, name in zip(metric_info["metrics"],
                                    metric_info["names"]):
                data = []
                for path_list, tag_list in zip(data_paths, data_tags):
                    def _create_tag(t):
                        ot = "{}_{}".format(t, metric_info["model"])
                        if metric_info["prefix"]:
                            ot = "{}_{}".format(
                                ot, metric_info["prefix"]
                            )
                        return ot

                    data.append([
                        os.path.join(
                            path,
                            metric_info["model"],
                            "{}_{}.nii.gz".format(_create_tag(tag), metric)
                        ) for path, tag in zip(path_list, tag_list)
                    ])

                subset_dict[model][name] = {}
                subset_dict[model][name]["tissue"] = \
                    _compute_cv_on(
                        data, tissue_segmentation,
                        {1: "csf", 2: "gm", 3: "wm"},
                        avg_rep
                    )
                subset_dict[model][name]["wm"] = \
                    _compute_cv_on(
                        data, wm_segmentation,
                        wm_labels, avg_rep
                    )

    return subset_dict


def create_cv_plot(
    data, axis, is_top, is_bottom, is_leftmost, is_rightmost, xlabel, ylabel
):
    ax = sns.barplot(
        x="CV", y="tissue",
        data=data, dodge=True, orient="h",
        ax=axis
    )

    ax.tick_params(which='minor', length=2)
    ax.grid(
        which='major', axis='x', color='lightgray',
        linestyle='--', linewidth=0.5
    )
    ax.minorticks_on()
    ax.grid(
        which='minor', axis='x', color='lightgray',
        linestyle='-.', linewidth=0.5, alpha=0.2
    )

    if is_top:
        ax.xaxis.set_label_position('top')
        ax.set_xlabel(xlabel)
        ax.tick_params(
            axis='x', which='both', bottom=False, top=True,
            labelbottom=False, labeltop=True, labelsize=7
        )
        ax.spines["bottom"].set_color("lightgray")
    elif is_bottom:
        ax.set_xlabel(xlabel)
        ax.tick_params(
            axis='x', which='both', bottom=True, top=False,
            labelbottom=True, labeltop=False, labelsize=7
        )
        ax.spines["top"].set_visible(False)
    else:
        ax.set_xlabel("")
        ax.tick_params(
            axis='x', which='both', bottom=False, top=False,
            labelbottom=False, labeltop=False
        )
        ax.spines["bottom"].set_color("lightgray")
        ax.spines["top"].set_visible(False)

    if is_leftmost:
        ax.tick_params(
            axis='y', which='both', left=False, right=False,
            labelleft=False, labelright=False
        )
        ax.set_ylabel(ylabel, rotation=30, ha='right', va='center')
    elif is_rightmost:
        ax.set_ylabel("")
        ax.tick_params(
            axis='y', which='both', left=False, right=False,
            labelleft=False, labelright=True
        )
    else:
        ax.set_ylabel("")
        ax.tick_params(
            axis='y', which='both', left=False, right=False,
            labelleft=False, labelright=False
        )
