#!/usr/bin/env python3

import argparse
import json
import multiprocessing
import os
import uuid
from multiprocessing import cpu_count

import nibabel as nib
import numpy as np
import pandas

_measures = {
    "dti_fa": {"dir": "dti", "suffix": "dti_fa", "type": "diffusion"},
    "dti_md": {"dir": "dti", "suffix": "dti_md", "type": "diffusion"},
    "dti_ad": {"dir": "dti", "suffix": "dti_ad", "type": "diffusion"},
    "dti_rd": {"dir": "dti", "suffix": "dti_rd", "type": "diffusion"},
    "fodf_afds": {"dir": "fodf", "suffix": "fodf_metrics_afds", "type": "diffusion"},
    "fodf_afdt": {"dir": "fodf", "suffix": "fodf_metrics_afdt", "type": "diffusion"},
    "fodf_nufo": {"dir": "fodf", "suffix": "fodf_metrics_nufo", "type": "diffusion"},
    "diamond_hei": {"dir": "diamond", "suffix": "diamond_hei", "type": "diffusion"},
    "diamond_heiAD": {"dir": "diamond", "suffix": "diamond_heiAD", "type": "diffusion"},
    "diamond_kappa": {"dir": "diamond", "suffix": "diamond_kappa", "type": "diffusion"},
    "diamond_kappaAD": {"dir": "diamond", "suffix": "diamond_kappaAD", "type": "diffusion"},
    "diamond_md": {"dir": "diamond", "suffix": "diamond_metrics_md", "type": "diffusion"},
    "diamond_ad": {"dir": "diamond", "suffix": "diamond_metrics_ad", "type": "diffusion"},
    "diamond_rd": {"dir": "diamond", "suffix": "diamond_metrics_rd", "type": "diffusion"},
    "diamond_fa": {"dir": "diamond", "suffix": "diamond_metrics_fa", "type": "diffusion"},
    "diamond_fw": {"dir": "diamond", "suffix": "diamond_metrics_fFW", "type": "diffusion"},
    "diamond_f": {"dir": "diamond", "suffix": "diamond_metrics_fractions", "type": "diffusion"},
    "diamond_ffa": {"dir": "diamond", "suffix": "diamond_metrics_max_ffa", "type": "diffusion"},
    "diamond_mose": {"dir": "diamond", "suffix": "diamond_mosemap", "type": "diffusion"},
    "t1_registration": {"dir": "", "suffix": "t1", "type": "anatomy"},
    "t1_wm-pvf": {"dir": "segmentation", "suffix": "wm_pvf", "type": "anatomy"},
    "t1_gm-pvf": {"dir": "segmentation", "suffix": "gm_pvf", "type": "anatomy"},
    "t1_csf-pvf": {"dir": "segmentation", "suffix": "csf_pvf", "type": "anatomy"}
}


def _create_parser():
    p = argparse.ArgumentParser()
    p.add_argument("repetitions_dir")
    p.add_argument("output_name")
    p.add_argument(
        "--measures", nargs="+",
        choices=list(_measures.keys()) + ["all"],
        default=["all"])
    p.add_argument("--save-json", action="store_true")
    p.add_argument("--save-to-latex", action="store_true")
    p.add_argument("--n-threads", default=cpu_count())

    return p


def _create_subject_description(root_dir):
    repetitions = os.listdir(root_dir)
    subjects_per_rep = [
        os.listdir(os.path.join(root_dir, r)) for r in repetitions]

    subjects = np.unique(list(s for r in subjects_per_rep for s in r))

    study_description = {
        "metadata": {
            "trace_denom_w": int(np.sum(
                list(len(s) for s in subjects_per_rep)) - 1),
            "trace_denom_ij": int(np.sum(
                list(len(s) - 1 for s in subjects_per_rep))),
            "subjects": {s: {} for s in subjects}
        },
        "data": {}
    }

    subjects_dict = study_description["metadata"]["subjects"]

    for i, repetition in enumerate(repetitions):
        for subject in subjects:
            if subject in subjects_per_rep[i]:
                datapoint_uuid = str(uuid.uuid4())
                subjects_dict[subject][repetition] = datapoint_uuid
                study_description["data"][datapoint_uuid] = os.path.join(
                    root_dir, repetition, subject
                )

    return study_description


def _load_data_plusD(path):
    img = nib.load(path)
    return img.get_fdata()[..., None]


def _compute_mean(subject, measure, repetition_roots):
    try:
        measure_dir, measure_suffix, _ = _measures[measure].values()

        return subject, measure, np.mean(np.concatenate(
            list(_load_data_plusD(
                os.path.join(
                    root, measure_dir,
                    "{}_{}.nii.gz".format(subject, measure_suffix)
                )
            ) for root in repetition_roots),
            axis=-1
        ), axis=-1)
    except BaseException as e:
        raise e


def _evaluate_reproducibility(measures, subjects_dict, pool_size=cpu_count()):

    subjects_means = {
        s: {} for s in subjects_dict["metadata"]["subjects"].keys()
    }

    reproducibility = {m: None for m in measures}

    with multiprocessing.Pool(pool_size) as pool:
        all_subjects_paths = [[
            (s, m, [subjects_dict["data"][u] for u in sd.values()])
            for s, sd in subjects_dict["metadata"]["subjects"].items()]
            for m in measures]

        all_subjects_paths = [ms for m in all_subjects_paths for ms in m]

        means_promise = pool.starmap_async(
            _compute_mean,
            all_subjects_paths
        )

        for subject, measure, mean in means_promise.get():
            subjects_means[subject][measure] = mean

    for measure in reproducibility.keys():
        data_mean = None
        for subject in subjects_means.keys():
            data = subjects_means[subject][measure][..., None]
            if data_mean is None:
                data_mean = data
            else:
                data_mean = np.concatenate((data_mean, data), axis=-1)

        full_mean = np.mean(data_mean, axis=-1)

        trkw, trkij = 0, 0
        for subject in subjects_dict["metadata"]["subjects"].keys():
            for rid in subjects_dict["metadata"]["subjects"][subject].values():
                data = nib.load(os.path.join(
                    subjects_dict["data"][rid], _measures[measure]["dir"],
                    "{}_{}.nii.gz".format(
                        subject, _measures[measure]["suffix"])
                )).get_fdata()

                trkw += np.sum((data - full_mean) ** 2., (0, 1, 2))
                trkij += np.sum(
                    (data - subjects_means[subject][measure]) ** 2., (0, 1, 2)
                )

        trkij = trkij[~np.isclose(trkw, 0.)]
        trkw = trkw[~np.isclose(trkw, 0.)]
        trkw /= subjects_dict["metadata"]["trace_denom_w"]
        trkij /= subjects_dict["metadata"]["trace_denom_ij"]
        reproducibility[measure] = np.mean(1. - trkij / trkw)

    return reproducibility


def _produce_reproducibility_df(reprod_dict):
    models = np.unique(list(k.split("_")[0] for k in reprod_dict.keys()))

    df_data = {}

    i = 0
    for model in models:
        for measure in \
                filter(lambda m: m.split("_")[0] == model, reprod_dict.keys()):

            df_data[i] = [
                _measures[measure]["type"], model,
                measure.split("_")[1], reprod_dict[measure]]
            i += 1

    return pandas.DataFrame.from_dict(
        df_data, orient='index', columns=["Type", "Model", "Measure", "I2C2"]
    ).sort_values(by=["Type", "Model", "Measure"])


if __name__ == "__main__":
    parser = _create_parser()
    args = parser.parse_args()

    measures = None
    if "all" in args.measures:
        measures = _measures.keys()
    else:
        measures = args.measures

    subjects_dict = _create_subject_description(args.repetitions_dir)

    if args.save_json:
        json.dump(
            subjects_dict,
            open("{}_subjects_description.json".format(args.output_name), "w+")
        )

    reprod = _evaluate_reproducibility(measures, subjects_dict, args.n_threads)

    if args.save_json:
        json.dump(
            reprod,
            open("{}_reproducibility.json".format(args.output_name), "w+")
        )

    dataframe = _produce_reproducibility_df(reprod)
    dataframe.to_csv("{}_reproducibility.csv".format(args.output_name))

    if args.save_to_latex:
        dataframe = dataframe.set_index(["Type", "Model"])
        dataframe.to_latex(
            "{}_reproducibility_latex.txt".format(args.output_name),
            multicolumn=True, bold_rows=True, float_format="%.3f")
