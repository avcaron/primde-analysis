#!/usr/bin/env python3

import argparse
import copy
import csv
import json
import multiprocessing
import os
import uuid

import matplotlib.pyplot as plt
import matplotlib.ticker as tck
import nibabel as nib
import numpy as np
import pandas as pd
import seaborn as sns

from functools import reduce
from tqdm import tqdm

from matplotlib.collections import PolyCollection

from lib.denoising_measures import get_cnr_table, get_snr_table
from lib.multiprocess_cv import (compute_subject_cv_multi,
                                 compute_subjects_cv_multi,
                                 get_sessions_data)
from lib.variation_coeff import extract_subset_cvs

models = {
    "dti": [{
        "model": "dti",
        "prefix": "",
        "metrics": ["md", "ad", "rd", "fa"],
        "names": ["MD", "AD", "RD", "FA"]
    }],
    "fodf": [{
        "model": "fodf",
        "prefix": "metrics",
        "metrics": ["afd", "afdt", "nufo"],
        "names": ["AFD", "AFDt", "NuFO"]
    }],
    "diamond_distribution": [{
        "model": "diamond",
        "prefix": "",
        "metrics": ["hei", "heiAD", "kappa", "kappaAD"],
        "names": ["hei", "heiAD", "kappa", "kappaAD"]
    }],
    "diamond_measures": [{
        "model": "diamond",
        "prefix": "metrics",
        "metrics": ["md", "fa", "max_ffa", "fFW"],
        "names": ["MD", "FA", "max fFA", "fFW"]
    }]
}


def _create_parser():
    p = argparse.ArgumentParser()

    p.add_argument("projects_root")
    p.add_argument("wm_labels")
    p.add_argument("--output-dir")
    p.add_argument("--input-json")
    p.add_argument("--save-json", action="store_true")
    p.add_argument("--save-tables", action="store_true")
    p.add_argument("--mean-intra", action="store_true")
    p.add_argument("--b0-thr", type=int, default=20)
    p.add_argument("--pre-cv")
    p.add_argument("--pre-snr")
    p.add_argument("--pre-cnr")
    p.add_argument("--global-max", action="store_true")
    p.add_argument("--processes", type=int, default=os.cpu_count())

    return p


def _load_labels(labels_file):
    labels = {}
    labels_meta = {}
    with open(labels_file) as handle:
        reader = csv.reader(handle)
        next(reader)
        for line in reader:
            labels[int(line[0])] = line[2]
            labels_meta[line[2]] = {
                "name": line[1],
                "hemisphere": line[3]
            }

    return labels, labels_meta


def _filter_directories(root):
    return {
        k: os.path.join(root, k) for k in filter(
            lambda it: os.path.isdir(os.path.join(root, it)),
            os.listdir(root)
        )
    }


def _filter_subjects_from_sessions(dir_dict):
    out_dict = {}
    sub_ses = {ss: ss.split("_") for ss in dir_dict.keys()}

    for subject in np.unique(list(ss[0] for ss in sub_ses.values())):
        data = dict(filter(lambda it: it[1][0] == subject, sub_ses.items()))
        out_dict[subject] = {
            v[1]: dir_dict[k] for k, v in data.items()
        }

    return out_dict


def _unpack_tables_to_json(dict_data):
    out_data = {}
    for k, v in dict_data.items():
        if isinstance(v, dict):
            out_data[k] = _unpack_tables_to_json(v)
        elif isinstance(v, pd.DataFrame):
            out_data[k] = v.to_dict()
        else:
            out_data[k] = v

    return out_data


def _generate_input_json(root):
    input_json = {}
    projects = _filter_directories(root)

    projects_json = {
        p: json.load(open(os.path.join(path, "metadata.json")))
        for p, path in projects.items()
    }

    input_json["metadata"] = {}
    input_json["metadata"]["projects"] = {
        k: projects_json[k] for k in projects.keys()
    }
    input_json["metadata"]["subjects"] = {}

    input_json["analysis"] = {
        "intra-subject": {k: [] for k in projects.keys()},
        "inter-subject": {k: [] for k in projects.keys()}
    }

    vendors = np.array(list(m["vendor"] for m in projects_json.values()))
    unique_vendors, vendor_counts = np.unique(vendors, return_counts=True)
    input_json["metadata"]["vendors"] = unique_vendors.tolist()

    multiple_vendors = len(unique_vendors) > 1
    multiple_site_per_vendor = any([cnt > 1 for cnt in vendor_counts])

    if multiple_vendors:
        input_json["analysis"]["inter-vendor"] = {
            k: [] for k in unique_vendors
        }

    if multiple_site_per_vendor:
        input_json["analysis"]["intra-vendor"] = {
            k: [] for k in unique_vendors[vendor_counts > 1]
        }

    input_json["datapoints"] = {}

    for project, project_dir in tqdm(projects.items(), "projects"):
        project_subject_ids = []

        subjects = _filter_subjects_from_sessions(
            _filter_directories(project_dir)
        )

        for subject, sessions in tqdm(subjects.items(), "subjects"):
            subject_id = str(uuid.uuid4())
            project_subject_ids.append(subject_id)

            input_json["metadata"]["subjects"][subject_id] = {
                "subject": subject,
                "project": project,
                "n_sessions": len(sessions.keys())
            }

            subject_session_ids, raw_subject_session_ids = [], []
            for session, session_dir in tqdm(sessions.items(), "sessions"):
                session_id = str(uuid.uuid4())
                subject_session_ids.append(session_id)

                input_json["datapoints"][session_id] = {
                    "session": session,
                    "data": session_dir
                }

            if len(sessions.keys()) > 1:
                input_json["analysis"]["intra-subject"][project].append(
                    subject_id
                )

            input_json["metadata"]["subjects"][subject_id]["sessions"] = \
                subject_session_ids

        input_json["metadata"]["projects"][project]["subjects"] = \
            project_subject_ids

        input_json["analysis"]["inter-subject"][project] = project_subject_ids

        project_vendor = projects_json[project]["vendor"]

        if multiple_vendors:
            input_json["analysis"]["inter-vendor"][project_vendor].extend(
                project_subject_ids
            )

        if (
            multiple_site_per_vendor and
            vendor_counts[unique_vendors == project_vendor] > 1
        ):
            input_json["analysis"]["intra-vendor"][project_vendor].extend(
                project_subject_ids
            )

    non_intra_subject_projects = []
    for project, subject_list in \
        input_json["analysis"]["intra-subject"].items():
        if len(subject_list) == 0:
            non_intra_subject_projects.append(project)

    for project in non_intra_subject_projects:
        input_json["analysis"]["intra-subject"].pop(project)

    if len(input_json["analysis"]["intra-subject"]) == 0:
        input_json["analysis"].pop("intra-subject")

    return input_json


def _plot_denoising_measure(
    datatable, category, measure, out_directory,
    prefix=None, row=None, col="project", aspect=1, orient=None,
    plot_type="violin", scatter_fn=sns.swarmplot, tight_layout=True,
    fig_size=None
):
    grid = sns.catplot(
        data=datatable, x=category, y=measure, row=row, col=col,
        hue="image", palette="mako", split=True, kind=plot_type,
        sharey=True, aspect=aspect, orient=orient, dropna=True
    )
    grid.set_xlabels(label='')

    if fig_size is not None:
        grid.fig.set_size_inches(*fig_size)

    for row in grid.axes:
        for ax in row:
            for poly in ax.collections:
                if isinstance(poly, PolyCollection):
                    poly.set_alpha(0.5)

        for ax in row[1:]:
            sns.despine(ax=ax, left=True)

    if scatter_fn:
        grid.map_dataframe(
            scatter_fn, x=category, y=measure,
            hue="image", palette="mako", dodge=True
        )

    if tight_layout:
        grid.fig.subplots_adjust(wspace=0)
        grid.tight_layout()

    grid.set_titles(col_template="{col_name}")

    out_name = "{}_{}.png".format(
        prefix, category if orient == "h" else measure
    ) if prefix else "{}.png".format(category if orient == "h" else measure)

    grid.savefig(os.path.join(out_directory, out_name))


def _unpack_noise_measure_dataframes(
    data_json, datatables, datatable_location, seg_column_name, measure_name
):
    dataframes = []
    for project, meta in data_json["metadata"]["projects"].items():
        for subject in meta["subjects"]:
            for session in datatables[subject].values():
                if not isinstance(session[datatable_location], pd.DataFrame):
                    data = pd.DataFrame(session[datatable_location])
                else:
                    data = session[datatable_location]
                data = data \
                    .rename_axis("image") \
                    .reset_index(level=0) \
                    .melt(
                        "image",
                        var_name=seg_column_name,
                        value_name=measure_name
                    )[[measure_name, "image", seg_column_name]]

                data.insert(3, "project", project)
                dataframes.append(data)

    datatable = pd.concat(dataframes)

    for df in dataframes:
        df["project"] = "whole"

    datatable = pd.concat([datatable] + dataframes)

    return datatable


def _compute_snr_plots(data_json, b0_thr, out_directory, save_tables=False):
    snr_tables = {}
    for subject_id, subject in tqdm(
        data_json["metadata"]["subjects"].items(),
        "per subject SNR"
    ):
        subject_snr_tables = {}
        for session_id in subject["sessions"]:
            session = data_json["datapoints"][session_id]

            if not os.path.isdir(os.path.join(session["data"], "raw")):
                pass

            tag = "_".join([subject["subject"], session["session"]])

            raw_prefix = os.path.join(session["data"], "raw", tag)

            raw_bvals = np.loadtxt("{}_dwi.bval".format(raw_prefix))
            raw_dwi = nib.load("{}_dwi.nii.gz".format(raw_prefix))

            processed_prefix = os.path.join(session["data"], tag)
            processed_bvals = np.loadtxt("{}_dwi.bval".format(processed_prefix))
            processed_dwi = nib.load("{}_dwi.nii.gz".format(processed_prefix))

            segmentation_prefix = os.path.join(
                session["data"], "segmentation", tag
            )
            tissue_segmentation = nib.load(
                "{}_segmentation.nii.gz".format(segmentation_prefix)
            ).get_fdata().astype(int)

            subject_snr_tables[session_id] = {
                "tissues": get_snr_table(
                    processed_dwi.get_fdata(), processed_bvals <= b0_thr,
                    raw_dwi.get_fdata(), raw_bvals <= b0_thr,
                    tissue_segmentation, {2: "gm", 3: "wm"}
                )
            }

        snr_tables[subject_id] = subject_snr_tables

    if save_tables:
        json.dump(
            _unpack_tables_to_json(snr_tables),
            open(os.path.join(out_directory, "snr_data.json"), "w+"),
            indent=4
        )

    return snr_tables


def _plot_snr(snr_tables, data_json, out_directory):
    datatable = _unpack_noise_measure_dataframes(
        data_json, snr_tables, "tissues", "tissue", "snr"
    )

    _plot_denoising_measure(
        datatable, "tissue", "snr", out_directory, "tissues"
    )


def _compute_cnr(
    data_json, b0_thr, wm_labels, out_directory, save_tables=False
):
    cnr_tables = {}
    for subject_id, subject in tqdm(
        data_json["metadata"]["subjects"].items(),
        "per subject CNR"
    ):
        subject_cnr_tables = {}
        for session_id in subject["sessions"]:
            session = data_json["datapoints"][session_id]

            if not os.path.isdir(os.path.join(session["data"], "raw")):
                pass

            tag = "_".join([subject["subject"], session["session"]])

            raw_prefix = os.path.join(session["data"], "raw", tag)

            raw_bvals = np.loadtxt("{}_dwi.bval".format(raw_prefix))
            raw_dwi = nib.load("{}_dwi.nii.gz".format(raw_prefix))

            processed_prefix = os.path.join(session["data"], tag)
            processed_bvals = np.loadtxt("{}_dwi.bval".format(processed_prefix))
            processed_dwi = nib.load("{}_dwi.nii.gz".format(processed_prefix))

            segmentation_prefix = os.path.join(
                session["data"], "segmentation", tag
            )
            wm_segmentation = nib.load(
                "{}_wm_segmentation_atlas.nii.gz".format(segmentation_prefix)
            ).get_fdata().astype(int)
            gm_mask = nib.load(
                "{}_gm_mask.nii.gz".format(segmentation_prefix)
            ).get_fdata().astype(bool)
            wm_mask = nib.load(
                "{}_wm_mask.nii.gz".format(segmentation_prefix)
            ).get_fdata().astype(bool)
            gm_fraction = nib.load(
                "{}_gm_pvf.nii.gz".format(segmentation_prefix)
            ).get_fdata()
            wm_fraction = nib.load(
                "{}_wm_pvf.nii.gz".format(segmentation_prefix)
            ).get_fdata()

            subject_cnr_tables[session_id] = {
                "whole_wm": get_cnr_table(
                    processed_dwi.get_fdata(), processed_bvals <= b0_thr,
                    raw_dwi.get_fdata(), raw_bvals <= b0_thr,
                    wm_mask.astype(int), {1: "wm"},
                    gm_mask, gm_fraction, wm_fraction
                ),
                "wm_segmentation": get_cnr_table(
                    processed_dwi.get_fdata(), processed_bvals <= b0_thr,
                    raw_dwi.get_fdata(), raw_bvals <= b0_thr,
                    wm_segmentation, wm_labels, gm_mask,
                    gm_fraction, wm_fraction
                )
            }

        cnr_tables[subject_id] = subject_cnr_tables

    if save_tables:
        json.dump(
            _unpack_tables_to_json(cnr_tables),
            open(os.path.join(out_directory, "cnr_data.json"), "w+"),
            indent=4
        )

    return cnr_tables


def _plot_cnr(cnr_tables, data_json, wm_labels, out_directory):
    datatable = _unpack_noise_measure_dataframes(
        data_json, cnr_tables, "whole_wm", "white matter", "cnr"
    )

    _plot_denoising_measure(
        datatable, "white matter", "cnr", out_directory, "wm"
    )

    datatable = _unpack_noise_measure_dataframes(
        data_json, cnr_tables, "wm_segmentation",
        "white matter segmentation", "cnr"
    )

    _plot_denoising_measure(
        datatable, "white matter segmentation", "cnr", out_directory,
        "wm_seg", fig_size=(
            len(wm_labels.keys()),
            30. * len(data_json["metadata"]["projects"].keys())
        ), row="project", col=None
    )


def _compute_max_cv(dictionary):
    max_cv_tissue, max_cv_wm = 0., 0.
    for k, v in dictionary.items():
        if k == "tissue":
            for t, cv in v.items():
                max_cv_tissue = max(max_cv_tissue, cv[0])
        elif k == "wm":
            for t, cv in v.items():
                max_cv_wm = max(max_cv_wm, cv[0])
        else:
            max_cv_tissue, max_cv_wm = _compute_max_cv(v)

    return max_cv_tissue, max_cv_wm


def _compute_cv_on_scalar_metrics(
    data_json, wm_labels, out_directory, save_tables=False,
    mean_intra=False, pool_size=1
):
    cv_dict = {
        "data": None,
        "metadata": {"tissue": {}, "wm": {}}
    }

    data_dict = {k: {} for k in data_json["analysis"].keys()}

    data_dict["inter-subject"] = {}

    subject_data = None
    vendor_data = None

    with multiprocessing.Pool(pool_size - 1 if pool_size > 1 else 1) as pool:
        projects_data = pool.starmap_async(
            compute_subjects_cv_multi,
            [(p, s, data_json, wm_labels, models)
             for p, s in data_json["analysis"]["inter-subject"].items()]
        )

        if "intra-subject" in data_json["analysis"]:
            intra_points = [
                [(p, s, data_json, wm_labels, models) for s in ss]
                for p, ss in data_json["analysis"]["intra-subject"].items()
            ]

            subject_data = pool.starmap_async(
                compute_subject_cv_multi,
                [pt for spt in intra_points for pt in spt]
            )

        if "intra-vendor" in data_json["analysis"]:
            vendor_data = pool.starmap_async(
                compute_subjects_cv_multi,
                [(v, s, data_json, wm_labels, models)
                 for v, s in data_json["analysis"]["intra-vendor"].items()]
            )

        if "inter-vendor" in data_json["analysis"]:
            if pool_size == 1:
                projects_data.wait()
                if subject_data is not None:
                    subject_data.wait()
                if vendor_data is not None:
                    vendor_data.wait()

            inter_vendor_tissue_segmentations = []
            inter_vendor_wm_segmentations = []
            inter_vendor_data_paths = []
            inter_vendor_data_tags = []
            for vendor, subjects in \
                    data_json["analysis"]["inter-vendor"].items():
                for subject in subjects:
                    sts, swm, sdp, sdt = get_sessions_data(subject, data_json)
                    inter_vendor_tissue_segmentations.append(sts)
                    inter_vendor_wm_segmentations.append(swm)
                    inter_vendor_data_paths.append(sdp)
                    inter_vendor_data_tags.append(sdt)

            data_dict["inter-vendor"] = extract_subset_cvs(
                inter_vendor_data_paths, inter_vendor_data_tags,
                inter_vendor_tissue_segmentations,
                inter_vendor_wm_segmentations,
                wm_labels, models
            )

        for p, cvs in projects_data.get():
            data_dict["inter-subject"]["inter-subject:{}".format(p)] = cvs

        if subject_data is not None:
            data_dict["intra-subject"] = {}

            if mean_intra:
                def _reduce_measures(acc, ms):
                    for model in acc.keys():
                        for measure in acc[model].keys():
                            for segmentation in acc[model][measure].keys():
                                acc[model][measure][segmentation] += \
                                    ms[model][measure][segmentation]

                    return acc

                cvs = [(t.split(":"), v) for t, v in subject_data.get()]
                for project in np.unique(list(t[0][0] for t in cvs)):
                    project_measures = list(cv[1] for cv in filter(
                        lambda tv: tv[0][0] == project, cvs
                    ))
                    n_subjects = float(len(project_measures))

                    measures_dict = reduce(
                        _reduce_measures,
                        project_measures[1:],
                        project_measures[0]
                    )

                    for model in measures_dict.keys():
                        for measure in measures_dict[model].keys():
                            for seg in measures_dict[model][measure].keys():
                                measures_dict[model][measure][seg] /= \
                                    n_subjects

                    data_dict["intra-subject"][
                        "mean intra-subject:{}".format(project)
                    ] = measures_dict
            else:
                for tag, cvs in subject_data.get():
                    data_dict["intra-subject"]["intra-subject:{}".format(
                        tag)] = cvs

        if vendor_data is not None:
            data_dict["intra-vendor"] = {}
            for v, cvs in vendor_data.get():
                data_dict["intra-vendor"]["intra-vendor:{}".format(v)] = cvs

    max_cv_t, max_cv_wm = _compute_max_cv(data_dict)

    cv_dict["data"] = data_dict
    cv_dict["metadata"]["tissue"]["labels"] = ["wm", "gm"]
    cv_dict["metadata"]["tissue"]["max_cv"] = max_cv_t
    cv_dict["metadata"]["wm"]["labels"] = list(wm_labels.values())
    cv_dict["metadata"]["wm"]["max_cv"] = max_cv_t

    if save_tables:
        json.dump(
            _unpack_tables_to_json(cv_dict),
            open(os.path.join(out_directory, "cv.json"), "w+"),
            indent=4
        )

    return cv_dict


def _create_cv_plot(
    data, axis, is_top, is_bottom, is_leftmost, is_rightmost,
    xlabel, ylabel, segmentation, color=None, color_palette=None,
    swap_ylabel_side=False
):

    color_conf = {}

    if color is not None:
        color_conf["color"] = color
    if color_palette is not None:
        color_conf["palette"] = color_palette

    ax = sns.barplot(
        x="CV", y=segmentation,
        data=data, dodge=True, orient="h",
        ax=axis, **color_conf
    )

    ax.tick_params(which='minor', length=2)
    ax.grid(
        which='major', axis='x', color='lightgray',
        linestyle='--', linewidth=0.5
    )
    ax.minorticks_on()
    ax.grid(
        which='minor', axis='x', color='lightgray',
        linestyle='-.', linewidth=0.5, alpha=0.2
    )

    if is_top:
        ax.xaxis.set_label_position('top')
        ax.set_xlabel(xlabel)
        ax.tick_params(
            axis='x', which='both', bottom=False, top=True,
            labelbottom=False, labeltop=True, labelsize=7
        )
        ax.spines["bottom"].set_color("lightgray")
    elif is_bottom:
        ax.set_xlabel(xlabel)
        ax.tick_params(
            axis='x', which='both', bottom=True, top=False,
            labelbottom=True, labeltop=False, labelsize=7
        )
        ax.spines["top"].set_visible(False)
    else:
        ax.set_xlabel("")
        ax.tick_params(
            axis='x', which='both', bottom=False, top=False,
            labelbottom=False, labeltop=False
        )
        ax.spines["bottom"].set_color("lightgray")
        ax.spines["top"].set_visible(False)

    if is_leftmost:
        ax.tick_params(
            axis='y', which='both', left=False, right=False,
            labelleft=True, labelright=False
        )
        if swap_ylabel_side:
            ax.set_ylabel("")
            ax.set_yticklabels(data["category"].tolist(), rotation=30)
        else:
            ax.set_ylabel(ylabel, rotation=30, ha='right', va='center')
    elif is_rightmost:
        if swap_ylabel_side:
            ax.tick_params(
                axis='y', which='both', left=False, right=False,
                labelleft=False, labelright=False
            )
            ax.set_ylabel(ylabel, rotation=270, labelpad=15)
            ax.yaxis.set_label_position("right")
        else:
            ax.tick_params(
                axis='y', which='both', left=False, right=False,
                labelleft=False, labelright=True
            )
            ax.set_ylabel("")
    else:
        ax.set_ylabel("")
        ax.tick_params(
            axis='y', which='both', left=False, right=False,
            labelleft=False, labelright=False
        )


def _plot_cv_row(
    metrics, segmentation, ax_row, row_title, is_top, is_bottom,
    color=None, color_palette=None, swap_ylabel_side=False
):
    max_cv = 0.
    for i, (metric, seg) in enumerate(metrics.items()):
        if not isinstance(seg[segmentation], pd.DataFrame):
            data = pd.DataFrame(seg[segmentation])
        else:
            data = seg[segmentation]

        data = data.melt(
            var_name=segmentation, value_name="CV"
        )

        max_cv = max(max_cv, data["CV"].max())

        _create_cv_plot(
            data, ax_row[i], is_top, is_bottom,
            i == 0, i == len(metrics) - 1,
            metric, row_title, segmentation,
            color, color_palette, swap_ylabel_side
        )

    return max_cv


def _split_labels(cv_dict, splitted_cv, model, segmentation):
    for project, sub_models in cv_dict.items():
        for metric, seg in sub_models[model].items():
            for label, data in seg[segmentation].items():
                splitted_cv[label][metric]["category"][
                    project.replace(":", " - ")
                ] = data

    return splitted_cv


def _plot_cv_split_labels(cv_dict, model, labels, segmentation, axs, n_plots):
    max_cv = 0

    metrics = [m["names"] for m in models[model]]
    metrics = [m for mm in metrics for m in mm]

    splitted_cv = {l: {m: {"category": {}} for m in metrics} for l in labels}
    splitted_cv = _split_labels(
        cv_dict["inter-subject"], splitted_cv, model, segmentation
    )

    if "intra-subject" in cv_dict:
        splitted_cv = _split_labels(
            cv_dict["intra-subject"], splitted_cv, model, segmentation
        )

    if "intra-vendor" in cv_dict:
        splitted_cv = _split_labels(
            cv_dict["intra-vendor"], splitted_cv, model, segmentation
        )

    if "inter-vendor" in cv_dict:
        splitted_cv = _split_labels(
            {"inter-vendor": cv_dict["inter-vendor"]},
            splitted_cv, model, segmentation
        )

    palette = sns.color_palette("bright", len(splitted_cv))

    for i, (label, metrics) in enumerate(splitted_cv.items()):
        cv = _plot_cv_row(
            metrics, "category", axs[i],
            label, i == 0, i == n_plots - 1,
            palette[i], swap_ylabel_side=True
        )
        max_cv = max(cv, max_cv)

    return round(max_cv + 0.005, 2)


def _plot_cv_merged(cv_dict, model, segmentation, axs, n_plots):
    ax_idx = 0
    max_cv = 0

    for project, p_models in cv_dict["inter-subject"].items():
        cv = _plot_cv_row(
            p_models[model], segmentation, axs[ax_idx],
            project.replace(":", "\n"), ax_idx == 0, ax_idx == n_plots - 1
        )

        max_cv = max(cv, max_cv)
        ax_idx += 1

    if "intra-subject" in cv_dict:
        for subject, s_models in cv_dict["intra-subject"].items():
            cv = _plot_cv_row(
                s_models[model], segmentation, axs[ax_idx],
                subject.replace(":", "\n"), False, ax_idx == n_plots - 1
            )

            max_cv = max(cv, max_cv)
            ax_idx += 1

    if "intra-vendor" in cv_dict:
        for v, (vendor, v_models) in enumerate(
                cv_dict["intra-vendor"].items()):
            cv = _plot_cv_row(
                v_models[model], segmentation, axs[ax_idx],
                vendor.replace(":", "\n"), False, ax_idx == n_plots - 1
            )

            max_cv = max(cv, max_cv)
            ax_idx += 1

    if "inter-vendor" in cv_dict:
        cv = _plot_cv_row(
            cv_dict["inter-vendor"][model], segmentation,
            axs[ax_idx], "inter-vendor", False, True
        )

        max_cv = max(cv, max_cv)

    return round(max_cv + 0.005, 2)


def _plot_model_cv(
    model, cv_dict, n_plots, segmentation,
    split_labels, global_max, out_directory, suffix
):
    mul_per_plot = 1.
    if split_labels:
        mul_per_plot = float(
            sum([
                len(cv_dict["data"]["inter-subject"]),
                len(cv_dict["data"]["intra-subject"])
                if "intra-subject" in cv_dict["data"] else 0,
                len(cv_dict["data"]["intra-vendor"])
                if "intra-vendor" in cv_dict["data"] else 0,
                1 if "inter-vendor" in cv_dict["data"] else 0
            ])
        )

    n_metrics = sum([len(m["metrics"]) for m in models[model]])
    fig, axs = plt.subplots(
        nrows=n_plots, ncols=n_metrics,
        sharex=True, sharey=True, dpi=360.,
        figsize=(1.5 * n_metrics + 1.0, 0.7 * n_plots + 1.0)
    )
    fig.subplots_adjust(hspace=0., wspace=0.1)

    if split_labels:
        max_cv = _plot_cv_split_labels(
            cv_dict["data"], model,
            cv_dict["metadata"][segmentation]["labels"],
            segmentation, axs, n_plots
        )
    else:
        max_cv = _plot_cv_merged(
            cv_dict["data"], model, segmentation, axs, n_plots
        )

    if global_max:
        max_cv = cv_dict["metadata"][segmentation]["max_cv"]

    for ax in axs.flatten():
        ax.set(xlim=(0., max_cv))
        if split_labels:
            ax.tick_params(axis='y', labelsize=26. / mul_per_plot, pad=0)

    for ax in axs[0, :]:
        ax.set_xticks([0., max_cv / 2., max_cv])
        ax.get_xticklabels()[0].set_ha('left')
        ax.get_xticklabels()[-1].set_ha('right')

    for ax in axs[-1, :]:
        ax.set_xticks([0., max_cv / 2., max_cv])
        ax.get_xticklabels()[0].set_ha('left')
        ax.get_xticklabels()[-1].set_ha('right')

    out_name = "{}_cv_{}".format(model, segmentation)
    if suffix:
        out_name = "{}_{}".format(out_name, suffix)

    fig.subplots_adjust(left=0.3, bottom=0.3)
    fig.savefig(os.path.join(out_directory, "{}.png".format(out_name)))


def _plot_cv(
    cv_dict, out_directory, segmentation="tissue", split_labels=False,
    global_max=False, pool_size=1, suffix=None
):
    data_dict = cv_dict["data"]
    n_plots = sum([
        len(data_dict["inter-subject"]),
        len(data_dict["intra-subject"]) if "intra-subject" in data_dict else 0,
        len(data_dict["intra-vendor"]) if "intra-vendor" in data_dict else 0,
        1 if "inter-vendor" in data_dict else 0
    ])

    if split_labels:
        n_plots = len(cv_dict["metadata"]["labels"][segmentation])

    with multiprocessing.Pool(pool_size)as pool:
        pool.starmap(
            _plot_model_cv,
            [(
                m, cv_dict, n_plots, segmentation, split_labels,
                global_max, out_directory, suffix
            ) for m in models.keys()]
        )


def generate_reports():
    parser = _create_parser()
    args = parser.parse_args()

    pool_size = args.processes

    out_dir = None
    if "output_dir" in args and args.output_dir:
        if not os.path.exists(args.output_dir) or \
           not os.path.isdir(args.output_dir):
            parser.error(
                "Output directory does not exists "
                "or is not a directory {}".format(args.output_dir)
            )
        else:
            out_dir = args.output_dir
    else:
        out_dir = os.path.join(os.getcwd(), "analysis")
        os.makedirs(out_dir, exist_ok=True)

    if "input_json" in args and args.input_json:
        data_json = json.load(open(args.input_json))
    else:
        if (
            ("pre_snr" in args and args.pre_snr) or
            ("pre_cnr" in args and args.pre_cnr) or
            ("pre_cv" in args and args.pre_cv)
        ):
            parser.error(
                "You must supply the input data json structure associated "
                "to the json files when using pre-computed tables"
            )

        data_json = _generate_input_json(args.projects_root)

        if args.save_json or args.save_tables:
            json.dump(
                data_json,
                open(os.path.join(out_dir, "input_data.json"), "w+"),
                indent=4, sort_keys=True
            )

    wm_labels, labels_meta = _load_labels(args.wm_labels)

    noise_measures_dir = os.path.join(out_dir, "noise_measures")
    os.makedirs(noise_measures_dir, exist_ok=True)

    if "pre_snr" in args and args.pre_snr:
        snr_data = json.load(open(args.pre_snr))
    else:
        snr_data = _compute_snr_plots(data_json, args.b0_thr,
                                      noise_measures_dir, args.save_tables)

    _plot_snr(snr_data, data_json, noise_measures_dir)

    if "pre_cnr" in args and args.pre_cnr:
        cnr_data = json.load(open(args.pre_cnr))
    else:
        cnr_data = _compute_cnr(data_json, args.b0_thr, wm_labels,
                                noise_measures_dir, args.save_tables)

    _plot_cnr(cnr_data, data_json, wm_labels, noise_measures_dir)

    cv_measures_dir = os.path.join(out_dir, "cv_measures")
    os.makedirs(cv_measures_dir, exist_ok=True)
    if "pre_cv" in args and args.pre_cv:
        cv_data = json.load(open(args.pre_cv))
    else:
        cv_data = _compute_cv_on_scalar_metrics(
            data_json, wm_labels, cv_measures_dir,
            args.save_tables, args.mean_intra, pool_size
        )

    _plot_cv(
        cv_data, cv_measures_dir, "tissue", False,
        args.mean_intra, args.global_max, pool_size
    )
    _plot_cv(
        copy.deepcopy(cv_data), cv_measures_dir, "tissue", True,
        args.global_max, pool_size, suffix="split_labels"
    )


if __name__ == "__main__":
    generate_reports()
